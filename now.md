---
layout: page
title: Now
permalink: /now/
---

I'm living in Stockholm, Sweden with my wife, son and dog.

I'm working in [Klarna Bank AB]("https://www.klarna.com/se/"). 

What I am up to now:

@Work
* Java
* AWS
* Kafka

@Home
* enjoying being a first-time father
* enjoying Stockholm's outdoors (while this fantastic Summer lasts)
* exercising
* cooking much more and trying different dishes
* reading